<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArticulos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_articulos', function($table){

			$table->increments('id');
			$table->string('titulo');
			$table->string('tema');
			$table->string('resumen');
			$table->longText('cuerpo');
			$table->integer('autor_id');
			$table->integer('categoria_id');
			$table->integer('referencia');
			$table->integer('lecturas');
			$table->enum('activo', array('0','1'))->default('1');
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_articulos');
	}

}
