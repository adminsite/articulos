<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArticulosAutores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_articulos_autores', function($table){

			$table->increments('id');
			$table->string('nombre');
			$table->string('apellido');
			$table->string('titulo');
			$table->enum('activo', array('0','1'))->default('1');
			$table->integer('usuario_id');
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_articulos_autores');
	}

}
