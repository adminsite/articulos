<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArticulosCategorias extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_articulos_categorias', function($table){

			$table->increments('id');
			$table->string('nombre')->unique();
			$table->string('url')->unique();
			$table->integer('lft');
			$table->integer('rgt');
			$table->integer('padre_id')->default('0');
			$table->integer('nivel')->default('1');
			$table->enum('activo', array('0','1'))->default('1');
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_articulos_categorias');
	}

}
