<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarProveedorVideo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('adm_articulos_videos', function(Blueprint $table)
		{
			$table->string('proveedor')->after('video_id');
			$table->string('imagen')->after('video_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('adm_articulos_videos', function(Blueprint $table)
		{
			$table->dropColum(array('proveedor', 'imagen'));
		});
	}

}
