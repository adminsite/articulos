<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArticulosVideo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_articulos_videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('url');
			$table->string('video_id');
			$table->integer('articulo_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_articulos_videos');
	}

}
