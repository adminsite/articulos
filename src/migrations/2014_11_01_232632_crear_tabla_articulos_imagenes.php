<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArticulosImagenes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_articulos_imagenes', function($table){

			$table->increments('id');
			$table->string('archivo');
			$table->string('directorio');
			$table->string('titulo');
			$table->mediumText('descripcion');
			$table->morphs('imageable');
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_articulos_imagenes');
	}

}
