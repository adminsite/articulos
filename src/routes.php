<?php

/**
 * Filtro para la comunicacion de la API
 *
 * @return Bool
 */
App::after(function($request, $response)
{
	if (Config::has('adm.cors')) {
		$response->header('Access-Control-Allow-Origin', Config::get('adm.cors'));
	}

	$response->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
	$response->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X_FILENAME, Content-Type, Content-Range, Content-Disposition, Content-Description');

	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		exit();
	}
});

Route::pattern('path', '[a-z0-9_\/]+');
Route::pattern('img', '[a-z0-9_]+(\.[a-z]{3})$');
Route::get('adm/pics/articulos/{path}/{img}', '\Adminsite\Articulos\Controllers\ImagenesController@index');

Route::group(array(
	'prefix'    => 'adm/api/v1', 
	'before'    => 'api', 
	'namespace' => '\Adminsite\Articulos\Controllers'
), function()
{
	Route::resource('articulos', 'ArticuloController');
	Route::resource('categorias', 'CategoriasController');
	
	//Imagenes
	Route::get('pic/articulos/{path}/{img}', 'ImagenesController@index');
	Route::delete('pic/articulos/{path}/{img}', 'ImagenesController@destroy');
});