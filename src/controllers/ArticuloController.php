<?php

namespace Adminsite\Articulos\Controllers;

use Adminsite\Articulos\Articulo,
	Adminsite\Articulos\Imagen,
	Adminsite\Articulos\Video;

use Input,
	Request,
	Response,
	DB,
	File;


class ArticuloController extends \BaseController
{
	private $response = array(
		'error'=>false
	);

	private $filesTypes = array('image/jpg', 'image/jpeg');

	private $path = 'adm/articulos/';

	public function __construct()
	{
		//Crear directorio de articulos si no existen
		if (File::isDirectory($this->path) == false) {
			File::makeDirectory($this->path, 0777, true, true);
		}

		$gitignore = 'adm/.gitignore';
		if (File::exists($gitignore) == false) {
			File::put($gitignore, '*');
		}
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$articulos = Articulo::with('imagenes', 'video')->paginate(15);
		return Response::json($articulos, '200');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store ()
	{
		DB::beginTransaction();

		try
		{
			$articulo = new Articulo;

			// Crear Nuevo
			$articulo->titulo       = strip_tags(Input::get('titulo'));
			$articulo->tema         = strip_tags(Input::get('tema'));
			$articulo->resumen      = strip_tags(Input::get('resumen', ''));
			$articulo->cuerpo       = Input::get('cuerpo');
			$articulo->autor_id     = Input::get('autor_id', 1);
			$articulo->categoria_id = Input::get('categoria_id', 0);
			$articulo->referencia   = strtotime(date('Y-m-d H:i:s'));

			//Validar
			if ($articulo->validate() == false)
			{
				throw new \Exception(json_encode($articulo->errors), 1);
			}

			$articulo->save();

			//Crear directorio para el slider
			$carpeta = 'art'.$articulo->referencia.'/';
			$dir     = $this->path . $carpeta;

			if (File::isDirectory($dir) == false) {
				File::makeDirectory($dir, 0777, true, true);
			}

			//Subir Archivos
			if (Input::hasFile('archivos')) {
				$imagenes = array();

				foreach (Input::file('archivos') as $archivo) {

					if ($archivo->isValid() == false and in_array($archivo->getMimeType(), $this->filesTypes) == false) {
						continue;
					}

					//Generar ID de imagen
					$bytes = openssl_random_pseudo_bytes(4);
					$hex   = bin2hex($bytes);

					$img = new Imagen;
					$img->archivo    = $hex.'.'.strtolower($archivo->getClientOriginalExtension());
					$img->directorio = $carpeta;

					$upload = $archivo->move($dir, $img->archivo);
					if ($upload) {
						$imagenes[] = $img;
					}
				}

				$articulo->imagenes()->saveMany($imagenes);
			}

			//Video
			if (Input::has('video_id')) {
				$video = new Video();
				$video->url       = Input::get('video_url');
				$video->titulo    = Input::get('video_titulo');
				$video->video_id  = Input::get('video_id');
				$video->imagen    = Input::get('video_imagen', '');
				$video->proveedor = Input::get('video_proveedor');

				$articulo->video()->save($video);
			}

			DB::commit();
			$articulo->imagenes = $articulo->imagenes()->get();
			return Response::json($articulo, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$articulo = Articulo::find($id);
		return Response::json($articulo, '200');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update ($id)
	{
		DB::beginTransaction();

		try
		{		
			//Seleccionar Registro
			$articulo = Articulo::where('id',$id)->with('video')->firstOrFail();
			$articulo->titulo       = strip_tags(Input::get('titulo', ''));
			$articulo->tema         = strip_tags(Input::get('tema', ''));
			$articulo->resumen      = strip_tags(Input::get('resumen', ''));
			$articulo->cuerpo       = Input::get('cuerpo', '');
			$articulo->categoria_id = Input::get('categoria_id', 0);

			//Validar
			if ($articulo->validate() == false)
			{
				throw new \Exception(json_encode($articulo->errors), 1);
			}

			$articulo->save();

			if (Input::hasFile('archivos')) {
				$carpeta  = 'art'.$articulo->referencia.'/';
				$dir      = $this->path . $carpeta;

				$imagenes = array();

				foreach (Input::file('archivos') as $archivo) {

					if ($archivo->isValid() == false and in_array($archivo->getMimeType(), $this->filesTypes) == false) {
						continue;
					}

					//Generar ID de imagen
					$bytes = openssl_random_pseudo_bytes(4);
					$hex   = bin2hex($bytes);

					$img = new Imagen;
					$img->archivo    = $hex.'.'.strtolower($archivo->getClientOriginalExtension());
					$img->directorio = $carpeta;

					$upload = $archivo->move($dir, $img->archivo);
					if ($upload) {
						$imagenes[] = $img;
					}
				}

				$articulo->imagenes()->saveMany($imagenes);
			}
				

			//Video
			if (Input::has('video_id')) {
				if (is_null($articulo->video)) {
					$video = new Video();
					$video->url       = Input::get('video_url');
					$video->titulo    = Input::get('video_titulo');
					$video->video_id  = Input::get('video_id');
					$video->imagen    = Input::get('video_imagen', '');
					$video->proveedor = Input::get('video_proveedor');

					$articulo->video()->save($video);
				} else {
					if (Input::get('video_url') != $articulo->video->url) {
						$video = $articulo->video;
						$video->url      = Input::get('video_url');
						$video->titulo   = Input::get('video_titulo');
						$video->video_id = Input::get('video_id');
						$video->imagen    = Input::get('video_imagen', '');
						$video->proveedor = Input::get('video_proveedor');

						$video->save();
					}
				}
			} else if(Input::has('video_url') == false and is_null($articulo->video) == false)
			{
				$articulo->video->delete();
			}

			DB::commit();
			$articulo->imagenes = $articulo->imagenes()->get();
			return Response::json($articulo, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy ($id)
	{
		DB::beginTransaction();

		try 
		{
			$articulo = Articulo::find($id);
			$articulo->imagenes()->delete();

			$dir = $this->path.'art'.$articulo->referencia;
			if (File::isDirectory($dir)) {
				//Borrar directorio
				File::deleteDirectory($dir);
			}

			//Eliminar Articulo
			$articulo->delete();

			DB::commit();
			return Response::json($this->response, '200');
		}
		catch (Exception $e)
		{
			DB::rollBack();
			$this->response['error'] = true;
			$this->response['mensaje'] = $e->getMessage();
			return Response::json($this->response, '400');
		}

	}
}
