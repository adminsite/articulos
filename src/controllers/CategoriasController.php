<?php

namespace Adminsite\Articulos\Controllers;

use Adminsite\Articulos\Categoria;
use \Illuminate\Support\Facades\Input,
	\Illuminate\Support\Facades\Request,
	\Illuminate\Support\Facades\Response,
	\Illuminate\Support\Facades\DB,
	\Illuminate\Support\Str;

class CategoriasController extends \Controller
{
	private $response = array(
		'error'=>false
	);

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(Categoria::all(), '200');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		DB::beginTransaction();

		try
		{
			$lft = Input::get('lft');
			$rgt = Input::get('rgt');
			
			Categoria::where('lft', '>', $rgt)->increment('lft', 2);
			Categoria::where('rgt', '>', $rgt)->increment('rgt', 2);

			$categoria = new Categoria;
			$categoria->nombre   = Input::get('nombre');
			$categoria->url      = Str::slug(Input::get('nombre'));
			$categoria->lft      = $lft;
			$categoria->rgt      = $rgt;
			$categoria->padre_id = 0;
			$categoria->nivel    = 1;

			if ($categoria->validate() == false)
			{
				throw new \Exception(json_encode($categoria->errors), 1);
			}

			$categoria->save();

			DB::commit();
			return Response::json($this->response, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		DB::beginTransaction();

		try
		{
			$categoria = Categoria::find($id);
			$categoria->nombre = Input::get('nombre');
			$categoria->url    = Str::slug(Input::get('nombre'));

			if ($categoria->validate() == false)
			{
				throw new \Exception(json_encode($categoria->errors), 1);
			}

			$categoria->save();

			DB::commit();
			return Response::json($this->response, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DB::beginTransaction();

		try
		{
			$categoria = Categoria::find($id);

			DB::table('ad_articulos_categorias')->where('lft', '>', $categoria->lft)->increment('lft', 2);
			DB::table('ad_articulos_categorias')->where('rgt', '>', $categoria->lft)->increment('rgt', 2);

			DB::commit();
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			return Response::json(array('error'=>$e->getMessage()), '400');
		}
	}


}
