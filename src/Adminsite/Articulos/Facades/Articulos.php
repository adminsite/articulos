<?php

namespace Adminsite\Articulos\Facades;

use Illuminate\Support\Facades\Facade;

class Articulos extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'articulos';
	}
}