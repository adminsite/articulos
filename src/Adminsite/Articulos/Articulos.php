<?php

namespace Adminsite\Articulos;

class Articulos
{
	/**
	 * Obtener los articulos de un autor
	 *
	 * @return Collection 
	 */
	public function autor($autor_id)
	{
		return Autor::find($autor_id)->articulos;
	}


	/**
	 * Devuelve los ultimos registros ingresados
	 *
	 * @param integer $cantidad optional
	 * @return object
	 */
	public function ultimos($cantidad = 5)
	{
		return Articulo::ultimos($cantidad)->with(array('imagenes'))->get();
	}


	/**
	 * Desvuelve los articulos mas leidos segun los ultimos dias
	 *
	 * @param integer $cantidad
	 * @param integer $dias
	 * @return object
	 */
	public function masLeidos($cantidad = 5, $dias = 5)
	{
		return Articulo::masLeidos($cantidad, $dias)->with('autor')->get();
	}


	/**
	 * Leer un articulo
	 *
	 * @param integer $id
	 * @return object
	 */
	public function leer($id)
	{
		$articulo = Articulo::where('id', $id);
		$articulo->increment('lecturas');

		return $articulo->with('imagenes')->first();
	}
}