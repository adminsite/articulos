<?php

namespace Adminsite\Articulos;

use \Illuminate\Database\Eloquent\Model as Eloquent;

/**
* Autor Modelo
*/
class Categoria extends Eloquent
{
	protected $table = 'adm_articulos_categorias';

	protected $fillable = array('nombre', 'url', 'lft', 'rgt');

	public static $reglas = array(
		'nombre' => 'required|unique:adm_articulos_categorias'
	);

	public static $mensajes = array(
		'nombre.required' => 'El nombre de la categoria es necesario.',
		'nombre.unique'   => 'Esa categoria ya existe'
	);

	/**
	 * Instancia Validador
	 * 
	 * @var Illuminate\Validation\Validators
	 */
	protected $validator;

	public function __construct(array $attributes = array(), Validator $validator = null)
	{
		parent::__construct($attributes);
		$this->validator = $validator ?: \App::make('validator');
	}

	/**
	 * Validates current attributes against reglas
	 */
	public function validate()
	{
		$v = $this->validator->make($this->attributes, static::$reglas, static::$mensajes);
		if ($v->passes())
		{
			return true;
		}
		$this->setErrors($v->messages());
		return false;
	}

	/**
	 * Set error message bag
	 * 
	 * @var Illuminate\Support\MessageBag
	 */
	protected function setErrors($errors)
	{
		$this->errors = $errors;
	}

	public function articulos()
	{
		return $this->hasMany('Articulo');
	}
}