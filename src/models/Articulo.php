<?php

namespace Adminsite\Articulos;

use \Illuminate\Validation\Validator, DB;
use \Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

/**
* Articulo Modelo
*/
class Articulo extends Eloquent
{
	//Tabla
	protected $table = 'adm_articulos';

	/**
	 * Reglas para la validacion
	 * 
	 * @var Array
	 */
	public static $reglas = array(
		'titulo'       => 'required|regex:/^[\p{L}\p{N}\s\p{P}]+$/u',
		'tema'         => 'required|regex:/^[\p{L}\p{N}\s]+$/u',
		'resumen'      => 'between:1,200',
		'cuerpo'       => 'required|between:1,5000',
		'autor_id'     => 'required|integer'
	);

	public static $mensajes = array(
		'titulo.required'       => 'El titulo del aritculo es requerido.',
		'titulo.regex'          => 'El titulo contiene caracteres invalidos.',
		'tema.required'         => 'Indique el tema que trata el articulo.',
		'tema.regex'            => 'El tema contiene caracteres invalidos.',
		'resumen.size'          => 'El resumen debe tener menos de 200 caracteres.',
		'cuerpo.required'       => 'El cuerpo del articulo es requerido.',
		'cuerpo.size'           => 'El cuerpo del articulo debe tener menos de 5000 caracteres.',
		'autor_id.required'     => 'No se indico el autor del Articulo.'
	);

	/**
	 * Instancia Validador
	 * 
	 * @var Illuminate\Validation\Validators
	 */
	protected $validator;

	public function __construct(array $attributes = array(), Validator $validator = null)
	{
		parent::__construct($attributes);
		$this->validator = $validator ?: \App::make('validator');
	}

	/**
	 * Validates current attributes against reglas
	 */
	public function validate()
	{
		$v = $this->validator->make($this->attributes, static::$reglas, static::$mensajes);
		if ($v->passes())
		{
			return true;
		}
		$this->setErrors($v->messages());
		return false;
	}

	/**
	 * Set error message bag
	 * 
	 * @var Illuminate\Support\MessageBag
	 */
	protected function setErrors($errors)
	{
		$this->errors = $errors;
	}
	
	/**
	 * Obtener imagenes del artculo
	 *
	 * @return Array
	 */
	public function imagenes ()
	{
		return $this->morphMany('\Adminsite\Articulos\Imagen', 'imageable');
	}

	public function getFirstImage ()
	{
		if ($this->imagenes->count() > 0) {
			return 'adm/pics/articulos/art'.$this->referencia.'/'.$this->imagenes->get(0)->archivo;
		} else {
			return 'adm/pics/articulos/art00/default.jpg';
		}
	}

	/**
	 * Autor del articulo
	 */
	public function autor ()
	{
		return $this->belongsTo('\Adminsite\Articulos\Autor');
	}

	/**
	 * Video del Articulo
	 */
	public function video ()
	{
		return $this->hasOne('\Adminsite\Articulos\Video');
	}

	/**
	 * Mas Leidas
	 * 
	 * @return Query Builder
	 */
	public function scopemasLeidos ($query, $cantidad = 5, $dias = 5)
	{
		$dt = Carbon::now();
		return $query->where('created_at', '>', $dt->subDays($dias))
					->orderBy('lecturas', 'desc')
					->take($cantidad);
	}


	/**
	 * Mas Leidas
	 * 
	 * @return Query Builder
	 */
	public function scopeUltimos ($query, $cantidad = 5)
	{
		return $query->orderBy('id', 'desc')->take($cantidad);
	}
}