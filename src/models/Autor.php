<?php

namespace Adminsite\Articulos;

use \Illuminate\Database\Eloquent\Model as Eloquent;

/**
* Autor Modelo
*/
class Autor extends Eloquent
{
	//Tabla
	protected $table = 'adm_articulos_autores';

	/**
	 * Obtener articulos del autor
	 *
	 * @return Array
	 */
	public function articulos ()
	{
		return $this->hasMany('\Adminsite\Articulos\Articulo', 'autor_id', 'id');
	}
}