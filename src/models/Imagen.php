<?php

namespace Adminsite\Articulos;

class Imagen extends \Illuminate\Database\Eloquent\Model
{
	//Tabla
	protected $table = 'adm_articulos_imagenes';

	protected $fillable = array('directorio', 'archivo');

	public function imageable()
    {
        return $this->morphTo();
    }
}